package main

import (
	"encoding/base64"
	"fmt"
	"io/fs"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"

	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-menu/v2/menu"
)

const (
	iconSrcDirPath = "icons/src/"
	iconOutputFile = "icons/icon.go"
)

func writeFile(f *os.File, s string) {
	_, err := f.WriteString(s)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	var (
		srcFiles   []fs.FileInfo
		outputFile *os.File
		err        error
	)

	if outputFile, err = os.Create(iconOutputFile); err != nil {
		log.Fatal(err)
	}
	defer outputFile.Close()

	writeFile(outputFile, "package icons\n")

	if srcFiles, err = ioutil.ReadDir(iconSrcDirPath); err != nil {
		log.Fatal(err)
	}

	for _, file := range srcFiles {
		var bytes []byte

		fileName := file.Name()

		if bytes, err = ioutil.ReadFile(path.Join(iconSrcDirPath, fileName)); err != nil {
			log.Fatal(err)
		}

		fileExt := filepath.Ext(fileName)
		fileNameWithoutExt := fileName[:len(fileName)-len(fileExt)]
		base64EncodedIcon := base64.StdEncoding.EncodeToString(bytes)

		fileEntry := fmt.Sprintf("var %s = \"%s\"\n", strings.Title(fileNameWithoutExt), base64EncodedIcon)
		writeFile(outputFile, fileEntry)
	}

	fmt.Println(menu.CodeFirstMenu)
}
