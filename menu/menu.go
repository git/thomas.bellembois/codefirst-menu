package menu

import (
	"bytes"
	"html/template"

	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-menu/v2/icons"
)

var CodeFirstMenu string

func init() {
	t := template.Must(template.New("").Funcs(template.FuncMap{
		"safe": func(s string) template.HTML { return template.HTML(s) },
	}).Parse(menuTemplate))

	data := struct {
		Background,
		Logo,
		LogoutIcon,
		ChangePasswordIcon,
		GitIcon,
		DroneIcon,
		SonarIcon,
		RegistryIcon,
		DockerIcon,
		DocIcon,
		PastebinIcon,
		ChatIcon,
		RunnerIcon,
		HelpIcon string
	}{
		Background:         icons.HomeBackground,
		Logo:               icons.Logo,
		LogoutIcon:         icons.Logout,
		ChangePasswordIcon: icons.LockReset,
		GitIcon:            icons.Git,
		DroneIcon:          icons.Flash,
		SonarIcon:          icons.Radar,
		RegistryIcon:       icons.Docker,
		DockerIcon:         icons.Api,
		DocIcon:            icons.FileDocumentEditOutline,
		PastebinIcon:       icons.ContentPaste,
		ChatIcon:           icons.Wechat,
		RunnerIcon:         icons.WebSync,
		HelpIcon:           icons.Help,
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, data); err != nil {
		panic(err)
	}

	CodeFirstMenu = tpl.String()
}
