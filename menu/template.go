package menu

var menuTemplate = `<div id="codefirstMenu" style="position: sticky; top: 0px; z-index: 10; display: inline-block; width: 100%; background-color: white;">

	<style type="text/css" scoped>
		a.codefirstMenu {
			color: black;
			text-decoration: none;
			font-size: 14px;
			font-family: arial;
			vertical-align: bottom;
		}
		img.codefirstMenu {
			border: 2px solid white;
			padding: 2px;
		}
		/*
		span.codefirstMenu {
			line-height: 4em;
		}
		*/
	</style>

	{{safe "<!--gitea"}}
	<style type="text/css" scoped>
		span#gitea > img {
			border: 2px solid #3DDC97;
			padding: 2px;
		}
	</style>
	gitea-->

	{{safe "<!--drone"}}
	<style type="text/css" scoped>
		span#drone > img {
			border: 2px solid #3DDC97;
			padding: 2px;
		}
	</style>
	drone-->

	{{safe "<!--sonar"}}
	<style type="text/css" scoped>
		span#sonar > img {
			border: 2px solid #3DDC97;
			padding: 2px;
		}
	</style>
	sonar-->

	{{safe "<!--dockerrunner"}}
	<style type="text/css" scoped>
		span#dockerrunner > img {
			border: 2px solid #3DDC97;
			padding: 2px;
		}
	</style>
	dockerrunner-->

	{{safe "<!--documentation"}}
	<style type="text/css" scoped>
		span#documentation > img {
			border: 2px solid #3DDC97;
			padding: 2px;
		}
	</style>
	documentation-->
	
	<div style="float: left; padding: 2px; background: white; width: 100%">

		<div title="home" style="float: left; margin-right: 50px;">
			<a class="codefirstMenu"  style="border-bottom: none;" href="https://CODEFIRST_HOSTNAME/home">
				<img class="" src="data:image/png;base64,{{.Logo}}"/>
			</a>	
		</div>

		<div title="gitea" style="float: left; margin-right: 10px;">
			<a class="codefirstMenu"  style="border-bottom: none;" href="https://CODEFIRST_HOSTNAME/git/user/oauth2/keycloak">
				<span id="gitea" class="codefirstMenu">
					<img class="codefirstMenu" src="data:image/png;base64,{{.GitIcon}}"/>
						&nbsp;
				</span>
			</a>
		</div>

		<div title="drone" style="float: left; margin-right: 10px;">
			<a class="codefirstMenu" style="border-bottom: none;" href="https://CODEFIRST_HOSTNAME/login">
				<span id="drone" class="codefirstMenu">
					<img class="codefirstMenu" src="data:image/png;base64,{{.DroneIcon}}"/>
						&nbsp;
				</span>
			</a>
		</div>

		<div title="sonarqube" style="float: left; margin-right: 10px;">
			<a class="codefirstMenu"  style="border-bottom: none;" href="https://CODEFIRST_HOSTNAME/sonar/sessions/init/oidc?return_to=%2Fsonar%2F">
				<span id="sonar" class="codefirstMenu">
					<img class="codefirstMenu" src="data:image/png;base64,{{.SonarIcon}}"/>	
						&nbsp;
				</span>
			</a>
		</div>
		
		<div title="documentation" style="float: left; margin-right: 50px;">
			<a class="codefirstMenu"  style="border-bottom: none;" href="https://CODEFIRST_HOSTNAME/documentation/">
				<span id="documentation" class="codefirstMenu">
					<img class="codefirstMenu" src="data:image/png;base64,{{.DocIcon}}"/>
						&nbsp;
				</span>
			</a>
		</div>

		<div title="docker runner" style="float: left; margin-right: 50px;">
			<a class="codefirstMenu"  style="border-bottom: none;" href="https://CODEFIRST_HOSTNAME/dockerrunner/index.html">
				<span id="dockerrunner" class="codefirstMenu">
					<img class="codefirstMenu" src="data:image/png;base64,{{.RunnerIcon}}"/>
						&nbsp;
				</span>
			</a>
		</div>

		<div title="cinny" style="float: left; margin-right: 10px;">
			<a class="codefirstMenu"  style="border-bottom: none;" target="_blank" href="https://CODEFIRST_HOSTNAME/cinny/">
				<span class="codefirstMenu">
					<img class="codefirstMenu" src="data:image/png;base64,{{.ChatIcon}}"/>
						&nbsp;
				</span>
			</a>
		</div>
	
		<div title="pastebin" style="float: left; margin-right: 10px;">
			<a class="codefirstMenu"  style="border-bottom: none;" target="_blank" href="https://CODEFIRST_HOSTNAME/pastebin/">
				<span class="codefirstMenu">
					<img class="codefirstMenu" src="data:image/png;base64,{{.PastebinIcon}}"/>
						&nbsp;
				</span>
			</a>
		</div>

		<div title="help" style="float: left; margin-right: 10px;">
			<a class="codefirstMenu"  style="border-bottom: none;" target="_blank" href="https://CODEFIRST_HOSTNAME/documentation/CodeFirst/docusaurus/GuidesTutorials/">
				<span class="codefirstMenu">
					<img class="codefirstMenu" src="data:image/png;base64,{{.HelpIcon}}"/>
						&nbsp;
				</span>
			</a>
		</div>

		
		<div title="logout" style="float: right; margin-right: 10px;">
			<a class="codefirstMenu"  style="border-bottom: none;" href="https://CODEFIRST_HOSTNAME/keycloak/realms/master/protocol/openid-connect/logout">
				<span class="codefirstMenu">
					<img class="codefirstMenu" src="data:image/png;base64,{{.LogoutIcon}}"/>
						&nbsp;
				</span>
			</a>
		</div>

	</div>

</div>
`
